package com.hz.optionalEquipmentService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OptionalEquipmentServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(OptionalEquipmentServiceApplication.class, args);
	}

}
