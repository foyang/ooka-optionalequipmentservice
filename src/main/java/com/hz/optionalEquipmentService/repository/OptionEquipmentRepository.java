package com.hz.optionalEquipmentService.repository;

import com.hz.optionalEquipmentService.model.OptionEquipment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OptionEquipmentRepository extends JpaRepository<OptionEquipment,Integer> {
    OptionEquipment findById(int id);
    OptionEquipment findByName(String name);
}
