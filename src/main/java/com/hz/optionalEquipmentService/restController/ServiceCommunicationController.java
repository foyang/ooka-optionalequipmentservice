package com.hz.optionalEquipmentService.restController;

import com.hz.optionalEquipmentService.payload.Equipment;
import com.hz.optionalEquipmentService.payload.FuelSystem;
import com.hz.optionalEquipmentService.payload.OilSystem;
import com.hz.optionalEquipmentService.payload.Valid;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@RestController
@CrossOrigin
@RequestMapping("service-equipment")
public class ServiceCommunicationController {
    @Value("${config.service.timout}")
    private Long duration;

    RestTemplate restTemplate = new RestTemplate();

    @PostMapping("add")
    public ResponseEntity<Equipment> saveEqupment(@RequestBody Equipment equipment){
        try {
            Thread.sleep(duration);
            HttpEntity<Equipment> request = new HttpEntity<>(equipment);
            restTemplate.postForEntity("http://localhost:9002/equipment/add", request, Equipment.class);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    @PostMapping("valid/oil")
    @HystrixCommand(fallbackMethod = "getFallBackOil")
    public ResponseEntity<Valid> checkValidOil(@RequestBody OilSystem oilSystem){
        try {
            Thread.sleep(duration);
            HttpEntity<OilSystem> request = new HttpEntity<>(oilSystem);
            return restTemplate.postForEntity("http://localhost:9003/oil-system/valid",request, Valid.class);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    return null;
    }

    @PostMapping("valid/fuel")
    public ResponseEntity<Valid> checkValidFuel(@RequestBody FuelSystem fuelSystem){
        try {
            Thread.sleep(duration);
            HttpEntity<FuelSystem> request = new HttpEntity<>(fuelSystem);
            return restTemplate.postForEntity("http://localhost:9004/fuel-system/valid",request,Valid.class);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getFallBackOil(){
        return "Papa jbjhhgjhvhj";
    }
}
